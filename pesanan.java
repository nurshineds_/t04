class pesanan extends kue{
    private double berat;

    public pesanan(String name, double price, double berat){ //create a constructor that contains 3 parameters as written in the main class
        super(name, price); //set the name and price to the superclass constructor
        setBerat(berat); //set the berat to the existing field
    }

    //basic setter and getter for berat
    public double getBerat() {
        return berat;
    }

    public void setBerat(double berat) {
        this.berat = berat;
    }

    //implementation of inherited abstract methods in superclass
    public double Berat(){
        return berat;
    }

    public double hitungHarga(){
        double harga = this.getPrice() * this.getBerat(); //harga is calculated based on the instruction that provided 
        return harga;
    }

    public double Jumlah(){
        double jumlah = 0;
        return jumlah;
    }
}