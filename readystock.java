class readystock extends kue{
    private double jumlah;

    public readystock(String name, double price, double jumlah){ //create a constructor that contains 3 parameters as written in the main class
        super(name, price); //set the name and price to the superclass constructor
        setJumlah(jumlah); //set the jumlah into existing field
    }

    //basic setter and getter for jumlah
    public double getJumlah() {
        return jumlah;
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    //implementation of inherited abstract methods in superclass
    public double Jumlah(){
        return jumlah;
    }

    public double hitungHarga(){
        double harga = this.getPrice() * this.getJumlah() * 2; //harga is calculated based on the instruction that provided
        return harga;
    }

    public double Berat(){
        double berat = 0;
        return berat;
    }
}